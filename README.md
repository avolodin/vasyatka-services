# Service Suite for Vasyatka Chat Bot

[What's the weather in Ryazan right now?](https://junky.solutions/weather/ryazan/today)

## Technology Stack

* Java
    - [Spring Boot](https://projects.spring.io/spring-boot/), both `1.5.whatever` and `2.0`
    - [Spring Cloud Kubernetes](https://github.com/spring-cloud-incubator/spring-cloud-kubernetes)
    - [Spring Cloud Sleuth](https://cloud.spring.io/spring-cloud-sleuth/)

* [Kubernetes](https://kubernetes.io/)

## What this is even

The system consists of:
* Nginx Web Server,
    - an entrypoint to the system from the internet
    - which is also  responsible for SSL termination
    - path mapping
    - and HTTP basic authentication (user default to `root` and password is `hackme`)
* Service Registry and Discovery (Provided by Kubernetes DNS and managed by the platform itself)
* Weather Service, which gets forecasts from different sources, *weather providers*.
* A couple of clients for different weather providers, which are used by *weather service* from above.
    - Meteo-Info Client
    - Open Weather Map Client
* Key-Value Service (can store arbitrary number of arguments for a given key, uses Redis HASHes)
* Postgres and Redis instances
* ELK

All that runs in a cluster managed by [Kubernetes](https://kubernetes.io/). Here, [a good video](https://www.youtube.com/watch?v=Bcs-inRnLDc) to get started with Kubernetes.

The whole suite can be deployed on [Google Cloud Platform](https://cloud.google.com/)
or on a cluster emulator such as [minikube](https://github.com/kubernetes/minikube).

GCE deployment is automated and handled by gitlab CI. See `.gitlab-ci.yml`.

## Picture Time

![Moving parts](diagrams/vasyatka.png)

## Painkillers

* [fabric8-maven-plugin](https://maven.fabric8.io/) generates `Dockerfile`s and k8s deployment descriptors
* [gitflow-maven-plugin](https://github.com/aleksandr-m/gitflow-maven-plugin) for easy branching.
Use it to create a hotfix branch, feature branch or perform a release.
* [Flyway](https://flywaydb.org/) postgres database versioning

## Development Guidelines

### Branching

- `develop`: primary development branch, branch off for `feature`s here. Each commit is buildable
- `feature/your-feature-name`: feature branches. Branch off `develop` and create a Merge Request back when finished
- `master`: each commit is a potential release, has a tag (`1.7`, for example)
and can be deployed to production by gitlab CI.
- `hotfix/hotfix-name`: fixes for a release, branch off `master` and then merge into both `master` and `develop`
- `release/release-number`: these are branched off `develop` and merged into `master`.
Created automatically by `gitflow-maven-plugin` because people tend to mess things up.

[gitflow-maven-plugin](https://github.com/aleksandr-m/gitflow-maven-plugin) makes branching and merging even easier
and thus is highly recommended.

### Easy Logging

By default each app logs to `stdout` using default pattern from Spring Boot.

If you also want to view logs in Kibana include a dependency with logback configuration file
```
<dependency>
    <groupId>com.epam.vasyatka</groupId>
    <artifactId>enable-elk</artifactId>
</dependency>
```
which enables logging to the Logstash server at `tcp://logstash:9200`.

To add Trace and Span IDs for easy request tracing across multiple services
add [Spring Cloud Sleuth](https://github.com/spring-cloud/spring-cloud-sleuth):
```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-sleuth</artifactId>
</dependency>
```

## How to Deploy to Google Cloud

The process is fully automatic, for details on CI steps see `.gitlab-ci.yml`.

A couple of steps are, however, manual and required only for the first-time setup (And remember, *You* can automate this!):

First of all a cluster has to be created, important settings are:

        zone "us-central1-a" (IOWA, aka the cheapest)
        cluster-version "1.7.*" (1.8 and above is not supported by fabric8 maven plugin an the time)
        machine-type "g1-small" (f1-micro struggles with heavy apps such as elasticsearch)
        num-nodes "3" (just enough CPU time) or more
        no-enable-cloud-logging (costs money)
        no-enable-cloud-monitoring (consts money)
        disable-addons HttpLoadBalancing (GCE ingress controller doesn't support url rewriting, but nginx does)

After cluster is ready, the next steps are:

* Setup kubectl to work with newly created cluster
* Deploy `nginx-ingress-controller` for Ingress resource, which is responsible for external access to the app
* Deploy `cert-manager` for automated SSL certificate renewal.
It can be deployed with `helm`, a package manager for k8s
* Create persistent disks for stateful app components.
* Deploy app itself with Gitlab CI
* Bind a static IP to the load balancer, so a domain can be point to it
* Create a DNS A record pointing to that IP
* After, and only after DNS is updated, request an SSL certificate for the first time

### Setup kubectl to work with newly created cluster

See `00-setup-kubectl.sh`.

### Deploy Nginx

See `01-nginx-ingress-controller.sh`.

### Install cert-manager with helm

1. For installing `helm`, see `02-helm.sh`.
2. For `cert-manger` itself see `03-cert-manager.sh`.

### Create persistent disks for stateful app components

See `04-persistent-disks.sh`.

### Deploy app with Gitlab CI

Deploy any existing release from Gitlab Web UI or make a new one with `gitflow-maven-plugin`.

### Bind a static IP to the load balancer

To promote automatically assigned IP to a static address see `05-bind-static-ip.sh`.

After that, create DNS Record:

    A Record    @    ${your-static-ip}

### Request an SSL certificate

See `06-request-ssl-certificate.sh` and `tls/junky-solutions-certificate.yml`.
Make changes to later if domain differs from `junky.solutions`.

After resource of `kind: Certificate` is deployed
and certificate was issued successfully it will be automatically renewed upon expiration.

## Local Deployment on Windows

[Getting Started with Kubernetes on your Windows Laptop with Minikube](https://rominirani.com/tutorial-getting-started-with-kubernetes-on-your-windows-laptop-with-minikube-3269b54a226).

- Install [Docker CE](https://store.docker.com/editions/community/docker-ce-desktop-windows)
- Install [Google Cloud SDK](https://cloud.google.com/sdk/)
- Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#download-as-part-of-the-google-cloud-sdk)
- Install [minikube](https://github.com/kubernetes/minikube#windows) **0.23.0** and add it to PATH
- Open "Hyper-V Manager", click on the "Virtual Switch Manager" and create new External Virtual Switch
with name "minikubeNAT".
- Open PowerShell ([you can't](https://docs.docker.com/machine/reference/env/) set env variables automatically
with cmd.exe) as Administrator and start the things:

        minikube start --kubernetes-version="v1.7.5" --vm-driver="hyperv" --hyperv-virtual-switch="minikubeNAT" --memory=4096 --alsologtostderr
        minikube addons enable heapster
        minikube addons enable ingress

        minikube docker-env | Invoke-Expression

        .\mvnw -Pdebug clean deploy

Now let's slow down a bit. To deploy a java app we have to follow a couple of steps:
1. compile sources and package artifacts (executable jar files)
2. build docker images with java runtime and artifacts
3. deploy those images to kubernetes cluster

Minikube runs in a VM, what means docker daemon inside it is not the same daemon which runs on your localhost.
But we want our freshly-built images be accessible inside a VM! Otherwise, how can we deploy them?

Luckily, we can tell `docker` command to communicate with any other machine just by setting a couple of environment
variables and `minikube` has a handy command to print them all at once.
For more information see [docker-machine env](https://docs.docker.com/machine/reference/env/).

That command is `minikube docker-env`, it can print variables in formats understandable by both unix and MS.

The line

    minikube docker-env | Invoke-Expresion

does exactly that. Minikube prints required variables in format understandable by
[Invoke-Expresion](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-expression?view=powershell-6)
which in turn evaluates them against **open terminal session**. That means you should execute it before running
`mvn fabric8:deploy`, `mvn fabric8:debug` or any other command that involves `docker` in a new terminal window.

Now let's continue and deploy other resources:

        kubectl create secret generic basic-auth --from-file=./k8s-resources/ingress/auth
        kubectl apply -f k8s-resources/ingress/ingress.yml

        kubectl create -f k8s-resources/postgres/disk-auto.yml
        kubectl create -f k8s-resources/postgres/deployment.yml
        kubectl create -f k8s-resources/postgres/svc.yml

        kubectl create -f k8s-resources/redis/disk-auto.yml
        kubectl create -f k8s-resources/redis/deployment.yml
        kubectl create -f k8s-resources/redis/svc.yml

        docker build migrations -t migrations:latest
        kubectl create -f migrations/deployment-local-image.yml

Migrations are incremental changes to a database schema. In our case they are managed by [flyway](https://flywaydb.org/).

In a local environment other than k8s you have to create schema with name ```weather``` and run migrations yourself.
Schema can be created with a ```createdb``` postgres utility or any other way.


Now all deployments

    minikube service key-value-service --url
    minikube service meteo-info-weather-client --url

and logs from stdout

    kubectl logs -f deployment/key-value-service

are accessible from command line via `kubectl` command or from browser UI,
which can be opened by executing `kubectl proxy` and then going to `http://127.0.0.1:8001/ui`.

To undeploy, execute `mvn fabric8:undeploy`.

One of convenient ways to call deployments is [HTTPie](https://httpie.org/) HTTP command line client and unix terminal:

    http -a root:hackme --verify=no https://$(minikube ip)/weather/ryazan/today
    echo '{ "location": "406" }' | http PATCH $(minikube service key-value-service --url)/vasya
    http $(minikube service key-value-service --url)/vasya/location

## Local Debugging

Remote Java Debugging with fabric8 maven plugin is simple enough. To attach debugger to a `key-value-service` execute:

    cd /key-value-service
    mvn fabric8:debug

## Local Deployment of ELK stack:

I have no idea why you might want this but here are commands, just in case:

    kubectl apply -f k8s-resources/elk/elastic.yml
    kubectl apply -f k8s-resources/elk/kibana.yml
    kubectl create secret generic logstash-config --from-file=./k8s-resources/elk/logstash.conf
    kubectl apply -f k8s-resources/elk/logstash.yml

Kibana will be live at `https://$(minikube ip)/kibana`. Don't try to access it directly.

## Local Deployment on Mac

It's mostly same as on Windows, start minikube with hyperkit backend

    brew install kubectl
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.23.0/minikube-darwin-amd64 \
        && chmod +x minikube \
        && sudo mv minikube /usr/local/bin/

    minikube start --kubernetes-version v1.7.5 --vm-driver hyperkit --memory 4096

Use eval to set docker environment variables to a terminal session and use maven wrapper to run maven

    eval $(minikube docker-env)
    ./mvnw -Pdebug clean deploy
