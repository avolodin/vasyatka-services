create table open_weather_map_locations (
    id serial,
    serviceId integer,
    name text
);

GRANT ALL PRIVILEGES ON TABLE open_weather_map_locations TO weather;