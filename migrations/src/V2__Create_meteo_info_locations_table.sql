create table meteo_info_locations (
    id serial,
    serviceId integer,
    name text
);

GRANT ALL PRIVILEGES ON TABLE meteo_info_locations TO weather;
