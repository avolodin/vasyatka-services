const fs = require('fs');
const contents = fs.readFileSync('meteoInfoResponse.txt', 'utf8'); // http://bulk.openweathermap.org/sample/city.list.json.gz
const lines = contents.split('\n');
const locations = lines.map(e => {
  const parts = e.split(';');
  return {
    serviceId: parts[0],
    name: parts[1].replace(/\'/g, '').toLowerCase(),
  }
});
const sql = locations.map(e => `insert into meteo_info_locations (serviceId, name) values ('${e.serviceId}', '${e.name}');`);
fs.writeFileSync('V3__Fill_meteo_info_locations_table.sql', sql.join('\n'));
