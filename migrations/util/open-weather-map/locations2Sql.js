const fs = require('fs');
const contents = fs.readFileSync('city.list.json'); // http://bulk.openweathermap.org/sample/city.list.json.gz
const json = JSON.parse(contents);
const locations = json.map(e => ({
  name: e.name.replace(/\'/g, '\'\''),
  serviceId: e.id,
}));
const sql = locations.map(e => `insert into open_weather_map_locations (serviceId, name) values ('${e.serviceId}', '${e.name}');`);
fs.writeFileSync('V5__Fill_open_weather_map_locations_table.sql', sql.join('\n'));
