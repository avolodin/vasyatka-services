package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "forecast")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class OpenWeatherMapResponseListWrapper {

	private List<OpenWeatherMapResponse> responses;

	@XmlElement(name = "time")
	public List<OpenWeatherMapResponse> getResponses() {
		return this.responses;
	}

	public void setResponses(List<OpenWeatherMapResponse> responses) {
		this.responses = responses;
	}

	public OpenWeatherMapResponseListWrapper() {
		super();
	}

	public OpenWeatherMapResponseListWrapper(List<OpenWeatherMapResponse> responses) {
		super();
		this.responses = responses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((responses == null) ? 0 : responses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpenWeatherMapResponseListWrapper other = (OpenWeatherMapResponseListWrapper) obj;
		if (responses == null) {
			if (other.responses != null)
				return false;
		} else if (!responses.equals(other.responses))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.client.openweathermap.connector.converter.OpenWeatherMapResponseListWrapper (responses="
				+ responses + ")";
	}

}
