package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class CommentResolver {
	private static final String CLEAR_SKY_DESCRIPTION = "Clear sky";
	private static final String FEW_CLOUDS_DESCRIPTION = "Few clouds";
	private static final String CLOUDY_DESCRIPTION = "Cloudy";
	private static final String THUNDER_DESCRIPTION = "Rain with Thunder";
	private static final String LIGHT_RAIN_DESCRIPTION = "Light Rain";
	private static final String RAIN_DESCRIPTION = "Rain";
	private static final String LIGHT_SNOW_DESCRIPTION = "Light Snow";
	private static final String PRECIPITATION_DESCRIPTION = "Precipitation";
	private static final String SMALL_PRECIPITATION_DESCRIPTION = "Small Precipitation";
	private static final String SNOW_DESCRIPTION = "Snow";
	private static final String HAIL_DESCRIPTION = "Possible Hail";
	private static final String NO_PRECIPITATION_DESCRIPTION = "No Precipitation";

	private final Map<String, String> cloudinessDescriptionByOwmCloudiness = new HashMap<String, String>() {
		{
			put("clear sky", CLEAR_SKY_DESCRIPTION);
			put("few clouds", FEW_CLOUDS_DESCRIPTION);
			put("scattered clouds", CLOUDY_DESCRIPTION);
			put("broken clouds", CLOUDY_DESCRIPTION);
			put("overcast clouds", CLOUDY_DESCRIPTION);
		}
	};

	public String byWeatherCodeAndCloudinessDescription(String owmCloudinessDescription, int weatherDescriptionCode) {
		String cloudinessDescription = cloudinessDescriptionByOwmCloudiness.get(owmCloudinessDescription);
		String precipitationDescription = NO_PRECIPITATION_DESCRIPTION;;
		String weatherDescription;
		if (!cloudinessDescription.equals(CLEAR_SKY_DESCRIPTION)) {
			if ((weatherDescriptionCode >= 200) && (weatherDescriptionCode <= 232)) {
				precipitationDescription = THUNDER_DESCRIPTION;
			} else if ((weatherDescriptionCode >= 300) && (weatherDescriptionCode <= 321)) {
				if ((weatherDescriptionCode <= 301) || (weatherDescriptionCode == 310)) {
					precipitationDescription = LIGHT_RAIN_DESCRIPTION;
				} else {
					precipitationDescription = RAIN_DESCRIPTION;
				}
			} else if ((weatherDescriptionCode >= 500) && (weatherDescriptionCode <= 531)) {
				if ((weatherDescriptionCode == 500) || (weatherDescriptionCode == 520)) {
					precipitationDescription = LIGHT_RAIN_DESCRIPTION;
				} else {
					precipitationDescription = RAIN_DESCRIPTION;
				}
			} else if ((weatherDescriptionCode >= 600) && (weatherDescriptionCode <= 622)) {
				if ((weatherDescriptionCode == 600) || (weatherDescriptionCode == 610)) {
					precipitationDescription = LIGHT_SNOW_DESCRIPTION;
				} else if ((weatherDescriptionCode == 611) || (weatherDescriptionCode == 612)
						|| (weatherDescriptionCode == 616)) {
					precipitationDescription = PRECIPITATION_DESCRIPTION;
				} else if (weatherDescriptionCode == 615) {
					precipitationDescription = SMALL_PRECIPITATION_DESCRIPTION;
				} else {
					precipitationDescription = SNOW_DESCRIPTION;
				}
			} else if (weatherDescriptionCode == 906) {
				precipitationDescription = HAIL_DESCRIPTION;
			}
			weatherDescription = String.format("%s, %s", cloudinessDescription, precipitationDescription);
		} else {
			weatherDescription = cloudinessDescription;
		}
		return weatherDescription;
	}
}
