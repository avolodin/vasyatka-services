package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "weatherdata")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class WeatherDataWrapper {

	private OpenWeatherMapResponseListWrapper openWeatherMapResponseListWrapper;

	@XmlElement(name = "forecast")
	public OpenWeatherMapResponseListWrapper getOpenWeatherMapResponseListWrapper() {
		return openWeatherMapResponseListWrapper;
	}

	public void setOpenWeatherMapResponseListWrapper(
			OpenWeatherMapResponseListWrapper openWeatherMapResponseListWrapper) {
		this.openWeatherMapResponseListWrapper = openWeatherMapResponseListWrapper;
	}

	public WeatherDataWrapper() {
		super();
	}

	public WeatherDataWrapper(OpenWeatherMapResponseListWrapper openWeatherMapResponseListWrapper) {
		super();
		this.openWeatherMapResponseListWrapper = openWeatherMapResponseListWrapper;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((openWeatherMapResponseListWrapper == null) ? 0 : openWeatherMapResponseListWrapper.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherDataWrapper other = (WeatherDataWrapper) obj;
		if (openWeatherMapResponseListWrapper == null) {
			if (other.openWeatherMapResponseListWrapper != null)
				return false;
		} else if (!openWeatherMapResponseListWrapper.equals(other.openWeatherMapResponseListWrapper))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.client.openweathermap.connector.converter.WeatherDataWrapper (openWeatherMapResponseListWrapper="
				+ openWeatherMapResponseListWrapper + ")";
	}

}
