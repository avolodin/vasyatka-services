package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement(name = "time")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class OpenWeatherMapResponse {

	private String date;

	private int weatherDescriptionCode;

	private Float minTemperature;

	private Float maxTemperature;

	private String temperatureUnit;

	private Float windSpeed;

	private String windSpeedName;

	private String windDirection;

	private Float pressure;

	private String pressureUnit;

	private Float precipitation;

	private String precipitationType;

	private Integer humidity;

	private String humidityUnit;

	private Integer cloudiness;

	private String cloudinessUnit;

	private String cloudinessName;

	@XmlAttribute(name = "from")
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@XmlPath("symbol/@number")
	public int getWeatherDescriptionCode() {
		return weatherDescriptionCode;
	}

	public void setWeatherDescriptionCode(int weatherDescriptio) {
		this.weatherDescriptionCode = weatherDescriptio;
	}

	@XmlPath("temperature/@min")
	public Float getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(Float minTemperature) {
		this.minTemperature = minTemperature;
	}

	@XmlPath("temperature/@max")
	public Float getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(Float maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	@XmlPath("temperature/@unit")
	public String getTemperatureUnit() {
		return temperatureUnit;
	}

	public void setTemperatureUnit(String temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}

	@XmlPath("windSpeed/@mps")
	public Float getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(Float windSpeed) {
		this.windSpeed = windSpeed;
	}

	@XmlPath("windSpeed/@name")
	public String getWindSpeedName() {
		return windSpeedName;
	}

	public void setWindSpeedName(String windSpeedName) {
		this.windSpeedName = windSpeedName;
	}

	@XmlPath("windDirection/@name")
	public String getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}

	@XmlPath("pressure/@value")
	public Float getPressure() {
		return pressure;
	}

	public void setPressure(Float pressure) {
		this.pressure = pressure;
	}

	@XmlPath("pressure/@unit")
	public String getPressureUnit() {
		return pressureUnit;
	}

	public void setPressureUnit(String pressureUnit) {
		this.pressureUnit = pressureUnit;
	}

	@XmlPath("precipitation/@value")
	public Float getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(Float precipitation) {
		this.precipitation = precipitation;
	}

	@XmlPath("precipitation/@type")
	public String getPrecipitationType() {
		return precipitationType;
	}

	public void setPrecipitationType(String precipitationType) {
		this.precipitationType = precipitationType;
	}

	@XmlPath("humidity/@value")
	public Integer getHumidity() {
		return humidity;
	}

	public void setHumidity(Integer humidity) {
		this.humidity = humidity;
	}

	@XmlPath("humidity/@unit")
	public String getHumidityUnit() {
		return humidityUnit;
	}

	public void setHumidityUnit(String humidityUnit) {
		this.humidityUnit = humidityUnit;
	}

	@XmlPath("clouds/@all")
	public Integer getCloudiness() {
		return cloudiness;
	}

	public void setCloudiness(Integer cloudiness) {
		this.cloudiness = cloudiness;
	}

	@XmlPath("clouds/@unit")
	public String getCloudinessUnit() {
		return cloudinessUnit;
	}

	public void setCloudinessUnit(String cloudinessUnit) {
		this.cloudinessUnit = cloudinessUnit;
	}

	@XmlPath("clouds/@value")
	public String getCloudinessName() {
		return cloudinessName;
	}

	public void setCloudinessName(String cloudinessName) {
		this.cloudinessName = cloudinessName;
	}

	OpenWeatherMapResponse() {
		super();
	}

	@java.beans.ConstructorProperties({ "date", "weatherDescriptionCode", "minTemperature", "maxTemperature",
			"temperatureUnit", "windSpeed", "windSpeedName", "windDirection", "pressure", "pressureUnit",
			"precipitation", "precipitationType", "humidity", "humidityUnit", "cloudiness", "cloudinessUnit",
			"cloudinessName" })
	OpenWeatherMapResponse(String date, int weatherDescriptionCode, Float minTemperature, Float maxTemperature,
			String temperatureUnit, Float windSpeed, String windSpeedName, String windDirection, Float pressure,
			String pressureUnit, Float precipitation, String precipitationType, Integer humidity, String humidityUnit,
			Integer cloudiness, String cloudinessUnit, String cloudinessName) {
		super();
		this.date = date;
		this.weatherDescriptionCode = weatherDescriptionCode;
		this.minTemperature = minTemperature;
		this.maxTemperature = maxTemperature;
		this.temperatureUnit = temperatureUnit;
		this.windSpeed = windSpeed;
		this.windSpeedName = windSpeedName;
		this.windDirection = windDirection;
		this.pressure = pressure;
		this.pressureUnit = pressureUnit;
		this.precipitation = precipitation;
		this.precipitationType = precipitationType;
		this.humidity = humidity;
		this.humidityUnit = humidityUnit;
		this.cloudiness = cloudiness;
		this.cloudinessUnit = cloudinessUnit;
		this.cloudinessName = cloudinessName;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.client.openweathermap.connector.converter.OpenWeatherMapResponse (date=" + date + ", weatherDescriptionCode=" + weatherDescriptionCode
				+ ", minTemperature=" + minTemperature + ", maxTemperature=" + maxTemperature + ", temperatureUnit="
				+ temperatureUnit + ", windSpeed=" + windSpeed + ", windSpeedName=" + windSpeedName + ", windDirection="
				+ windDirection + ", pressure=" + pressure + ", pressureUnit=" + pressureUnit + ", precipitation="
				+ precipitation + ", precipitationType=" + precipitationType + ", humidity=" + humidity
				+ ", humidityUnit=" + humidityUnit + ", cloudiness=" + cloudiness + ", cloudinessUnit=" + cloudinessUnit
				+ ", cloudinessName=" + cloudinessName + ")";
	}

	public static class OpenWeatherMapResponseBuilder {
		private String date;

		private int weatherDescriptionCode;

		private Float minTemperature;

		private Float maxTemperature;

		private String temperatureUnit;

		private Float windSpeed;

		private String windSpeedName;

		private String windDirection;

		private Float pressure;

		private String pressureUnit;

		private Float precipitation;

		private String precipitationType;

		private Integer humidity;

		private String humidityUnit;

		private Integer cloudiness;

		private String cloudinessUnit;

		private String cloudinessName;

		public OpenWeatherMapResponseBuilder() {
		}

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder date(String date) {
			this.date = date;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder weatherDescriptionCode(int weatherDescriptionCode) {
			this.weatherDescriptionCode = weatherDescriptionCode;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder minTemperature(Float minTemperature) {
			this.minTemperature = minTemperature;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder maxTemperature(Float maxTemperature) {
			this.maxTemperature = maxTemperature;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder temperatureUnit(String temperatureUnit) {
			this.temperatureUnit = temperatureUnit;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder windSpeed(Float windSpeed) {
			this.windSpeed = windSpeed;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder windSpeedName(String windSpeedName) {
			this.windSpeedName = windSpeedName;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder windDirection(String windDirection) {
			this.windDirection = windDirection;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder pressure(Float pressure) {
			this.pressure = pressure;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder pressureUnit(String pressureUnit) {
			this.pressureUnit = pressureUnit;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder precipitation(Float precipitation) {
			this.precipitation = precipitation;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder precipitationType(String precipitationType) {
			this.precipitationType = precipitationType;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder humidity(Integer humidity) {
			this.humidity = humidity;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder humidityUnit(String humidityUnit) {
			this.humidityUnit = humidityUnit;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder cloudiness(Integer cloudiness) {
			this.cloudiness = cloudiness;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder cloudinessUnit(String cloudinessUnit) {
			this.cloudinessUnit = cloudinessUnit;
			return this;
		};

		public OpenWeatherMapResponse.OpenWeatherMapResponseBuilder cloudinessName(String cloudinessName) {
			this.cloudinessName = cloudinessName;
			return this;
		};

		public OpenWeatherMapResponse build() {
			return new OpenWeatherMapResponse(date, weatherDescriptionCode, minTemperature, maxTemperature, temperatureUnit,
					windSpeed, windSpeedName, windDirection, pressure, pressureUnit, precipitation, precipitationType,
					humidity, humidityUnit, cloudiness, cloudinessUnit, cloudinessName);
		}

		@Override
		public String toString() {
			return "com.epam.vasyatka.weather.client.openweathermap.connector.converter.OpenWeatherMapRespone.OpenWeatherMapResponseBuilder (date=" + date + ", weatherDescriptionCode=" + weatherDescriptionCode
					+ ", minTemperature=" + minTemperature + ", maxTemperature=" + maxTemperature + ", temperatureUnit="
					+ temperatureUnit + ", windSpeed=" + windSpeed + ", windSpeedName=" + windSpeedName
					+ ", windDirection=" + windDirection + ", pressure=" + pressure + ", pressureUnit=" + pressureUnit
					+ ", precipitation=" + precipitation + ", precipitationType=" + precipitationType + ", humidity="
					+ humidity + ", humidityUnit=" + humidityUnit + ", cloudiness=" + cloudiness + ", cloudinessUnit="
					+ cloudinessUnit + ", cloudinessName=" + cloudinessName + ")";
		}
		
	}
}