package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import com.epam.vasyatka.weather.integration.connector.ProviderLocationMapper;
import com.epam.vasyatka.weather.integration.model.exception.LocationNotSupportedException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class OpenWeatherMapLocationMapper implements ProviderLocationMapper {

	public static final String GET_SERVICE_ID_BY_NAME = "SELECT SERVICEID FROM OPEN_WEATHER_MAP_LOCATIONS WHERE LOWER(NAME) = ? LIMIT 1";

	private final JdbcTemplate jdbc;

	public OpenWeatherMapLocationMapper(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	@Override
	public String id(String location) {
		try {
			Integer id = jdbc.queryForObject(GET_SERVICE_ID_BY_NAME, Integer.class, location.toLowerCase());
			return id.toString();
		} catch (DataAccessException ex) {
			throw new LocationNotSupportedException(location, ex);
		}
	}
}
