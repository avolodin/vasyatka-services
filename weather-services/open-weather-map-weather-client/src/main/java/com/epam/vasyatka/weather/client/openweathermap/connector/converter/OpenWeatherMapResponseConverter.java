package com.epam.vasyatka.weather.client.openweathermap.connector.converter;

import com.epam.vasyatka.weather.integration.model.CloudsInfo;
import com.epam.vasyatka.weather.integration.model.Forecast;
import com.epam.vasyatka.weather.integration.model.HumidityInfo;
import com.epam.vasyatka.weather.integration.model.PrecipitationInfo;
import com.epam.vasyatka.weather.integration.model.PressureInfo;
import com.epam.vasyatka.weather.integration.model.TemperatureInfo;
import com.epam.vasyatka.weather.integration.model.WindInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import java.io.StringReader;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

@Service
public class OpenWeatherMapResponseConverter {

	private final CommentResolver descriptionResolver;

	public OpenWeatherMapResponseConverter(CommentResolver descriptionResolver) {
		super();
		this.descriptionResolver = descriptionResolver;
	}

	public List<Forecast> today(String response) {
		try {
			List<OpenWeatherMapResponse> rawResponse = call(response);
			Stream<Forecast> convertedResponse = rawResponse.stream().map(this::convert);
			return convertedResponse.filter(element -> element.getDate().truncatedTo(ChronoUnit.DAYS)
					.equals(Instant.now().truncatedTo(ChronoUnit.DAYS))).collect(Collectors.toList());
		} catch (RuntimeException ex) {
			throw new RuntimeException("Couldn't convert response from the provider", ex);
		}
	}

	private List<OpenWeatherMapResponse> call(String response) {
		XMLStreamReader xmlStreamReader = null;
		try (StringReader stringReader = new StringReader(response)) {
			JAXBContext jaxbContext = JAXBContext.newInstance(WeatherDataWrapper.class);
			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
			xmlStreamReader = xmlInputFactory.createXMLStreamReader(stringReader);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			WeatherDataWrapper responsesWrapper = (WeatherDataWrapper) jaxbUnmarshaller.unmarshal(xmlStreamReader);
			List<OpenWeatherMapResponse> responses = responsesWrapper.getOpenWeatherMapResponseListWrapper()
					.getResponses();
			return responses;
		} catch (JAXBException e) {
			throw new RuntimeException("Couldn't convert response from the provider", e);
		} catch (XMLStreamException e) {
			throw new RuntimeException("Malformed response: " + response);
		} finally {
			try {
				xmlStreamReader.close();
			} catch (XMLStreamException e) {
				throw new RuntimeException("Malformed response: " + response);
			}
		}
	}

	private Forecast convert(OpenWeatherMapResponse response) {
		Instant date = LocalDateTime.parse(response.getDate()).toInstant(ZoneOffset.UTC);
		TemperatureInfo temperature = new TemperatureInfo(response.getMinTemperature(), response.getMaxTemperature(),
				response.getTemperatureUnit());
		WindInfo windInfo = new WindInfo(response.getWindSpeed(), null, response.getWindDirection(), "mps");
		PressureInfo pressureInfo = new PressureInfo(response.getPressure(), null, response.getPressureUnit());
		PrecipitationInfo precipitationInfo = new PrecipitationInfo(response.getPrecipitation(), null,
				response.getPrecipitationType(), null, null);
		HumidityInfo humidityInfo = new HumidityInfo(response.getHumidity(), response.getHumidityUnit());
		CloudsInfo cloudsInfo = new CloudsInfo(response.getCloudiness(), response.getCloudinessUnit(),
				response.getCloudinessName());
		String weatherDescription = descriptionResolver.byWeatherCodeAndCloudinessDescription(
				cloudsInfo.getDescription(), response.getWeatherDescriptionCode());
		return Forecast.builder().date(date).weatherDescription(weatherDescription).temperature(temperature)
				.wind(windInfo).pressure(pressureInfo).precipitation(precipitationInfo).humidity(humidityInfo)
				.clouds(cloudsInfo).build();
	}

	public List<Forecast> week(String response) {
		try {
			List<OpenWeatherMapResponse> raw = call(response);
			return raw.stream().map(this::convert).collect(Collectors.toList());
		} catch (RuntimeException ex) {
			throw new RuntimeException("Couldn't convert response from the provider", ex);
		}
	}
}
