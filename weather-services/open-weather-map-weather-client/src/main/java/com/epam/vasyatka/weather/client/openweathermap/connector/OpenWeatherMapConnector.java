package com.epam.vasyatka.weather.client.openweathermap.connector;

import com.epam.vasyatka.weather.client.openweathermap.connector.converter.OpenWeatherMapResponseConverter;
import com.epam.vasyatka.weather.integration.connector.ProviderConnector;
import com.epam.vasyatka.weather.integration.connector.ProviderLocationMapper;
import com.epam.vasyatka.weather.integration.model.Forecast;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class OpenWeatherMapConnector implements ProviderConnector {

	private final OpenWeatherMapResponseConverter converter;

	private final ProviderLocationMapper locationMapper;

	private final RestTemplate template;

	@Value("${provider.endpoint}")
	private String endpoint;

	public OpenWeatherMapConnector(OpenWeatherMapResponseConverter converter, ProviderLocationMapper locationMapper,
			RestTemplate template) {
		this.converter = converter;
		this.locationMapper = locationMapper;
		this.template = template;
	}

	@Override
	public List<Forecast> today(String location) {
		String uri = endpoint.replace("@{id}", locationMapper.id(location));
		String response = template.getForObject(uri, String.class);
		return converter.today(response);
	}

	@Override
	public List<Forecast> week(String location) {
		String uri = endpoint.replace("@{id}", locationMapper.id(location));
		String response = template.getForObject(uri, String.class);
		return converter.week(response);
	}
}
