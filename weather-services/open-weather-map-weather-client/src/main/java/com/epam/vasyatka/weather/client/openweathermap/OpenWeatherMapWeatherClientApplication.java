package com.epam.vasyatka.weather.client.openweathermap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenWeatherMapWeatherClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenWeatherMapWeatherClientApplication.class, args);
	}
}
