package com.epam.vasyatka.weather.gateway;

import com.epam.vasyatka.weather.gateway.clients.ClientRepository;
import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientCaller {

	private final ClientRepository clientRepository;

	private final RestTemplate restTemplate;

	private final ObjectMapper objectMapper;

	@Value("${service.client.identifier_header}")
	private String serviceIdHeader;

	public ClientCaller(ClientRepository clientRepository, RestTemplate restTemplate, ObjectMapper objectMapper) {
		this.clientRepository = clientRepository;
		this.restTemplate = restTemplate;
		this.objectMapper = objectMapper;
	}

	/* Maybe we can create another value object? This generic pair is unreadable */
	public <T> Pair<Map<String, T>, Map<String, ClientServiceErrorResponse>> call(String endpoint, Class<T> type) {
		List<String> services = clientRepository.getServiceIdentifiers();
		if (services.isEmpty()) {
			throw new RuntimeException("No client services are registered");
		}

		List<ResponseEntity<String>> response = services.stream()
				.map(id -> "http://" + id + endpoint)
				.map(this::callClient)
				.filter(this::isValidClientResponse)
				.collect(Collectors.toList());
		if (response.size() == 0) {
			throw new RuntimeException("No client services are registered");
		}

		Map<String, T> details = getDetails(response, type);
		Map<String, ClientServiceErrorResponse> errors = getErrors(response);
		return new Pair<>(details, errors);
	}

	private ResponseEntity<String> callClient(String uri) {
		try {
			return restTemplate.getForEntity(uri, String.class);
		} catch (ResourceAccessException ex) {
			return null;
		}
	}

	private boolean isValidClientResponse(ResponseEntity<String> entity) {
		return entity != null
				&& !entity.getHeaders().get(serviceIdHeader).isEmpty()
				&& entity.getBody() != null;
	}

	private <T> Map<String, T> getDetails(List<ResponseEntity<String>> response, Class<T> type) {
		return response.stream()
				.filter(entity -> entity.getStatusCode().is2xxSuccessful())
				.map(entity -> {
					String body = entity.getBody();
					try {
						T forecast = objectMapper.readValue(body, type);
						String clientName = getServiceName(entity);
						return Collections.singletonMap(clientName, forecast);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				})
				.collect(HashMap<String, T>::new, Map::putAll, Map::putAll);
	}

	private Map<String, ClientServiceErrorResponse> getErrors(List<ResponseEntity<String>> response) {
		return response.stream()
				.filter(entity -> {
					HttpStatus statusCode = entity.getStatusCode();
					return statusCode.is4xxClientError() || statusCode.is5xxServerError();
				})
				.map(entity -> {
					String body = entity.getBody();
					try {
						ClientServiceErrorResponse error = objectMapper.readValue(body, ClientServiceErrorResponse.class);
						String clientName = getServiceName(entity);
						return Collections.singletonMap(clientName, error);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				})
				.collect(HashMap<String, ClientServiceErrorResponse>::new, Map::putAll, Map::putAll);
	}

	private String getServiceName(ResponseEntity<String> entity) {
		List<String> providedBy = entity.getHeaders().get(serviceIdHeader);
		return Optional.of(providedBy.get(0)).orElse("Unknown");
	}
}
