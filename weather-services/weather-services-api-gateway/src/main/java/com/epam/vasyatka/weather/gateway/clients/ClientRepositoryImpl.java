package com.epam.vasyatka.weather.gateway.clients;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
class ClientRepositoryImpl implements ClientRepository {

	private final DiscoveryClient client;

	public ClientRepositoryImpl(DiscoveryClient client) {
		this.client = client;
	}

	@Override
	public List<String> getServiceIdentifiers() {
		List<String> services = client.getServices();
		List<String> clients = services.stream()
				.filter(this::isWeatherProviderClient)
				.collect(Collectors.toList());
		return clients;
	}

	private boolean isWeatherProviderClient(String id) {
		return id.toLowerCase().endsWith("weather-client");
	}
}
