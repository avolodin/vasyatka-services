package com.epam.vasyatka.weather.gateway;

import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.concurrent.TimeoutException;

@RestControllerAdvice
public class WeatherGatewayExceptionHandler {

	@ExceptionHandler(TimeoutException.class)
	@ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
	public ClientServiceErrorResponse timeout(TimeoutException ex) {
		return ClientServiceErrorResponse.builder()
				.error("Request to the client timed out - to be implemented")
				.description(ex.getMessage())
				.build();
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	public ClientServiceErrorResponse noClients(RuntimeException ex) {
		return ClientServiceErrorResponse.builder()
				.error("Gateway error")
				.description(ex.getMessage())
				.build();
	}
}
