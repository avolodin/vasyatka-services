package com.epam.vasyatka.weather.gateway.model;

import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import com.epam.vasyatka.weather.integration.model.Forecast;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiGatewayMultipleResponse {

	private final Forecast[] summary;

	private final Map<String, Forecast[]> detail;

	private final Map<String, ClientServiceErrorResponse> errors;

	@java.beans.ConstructorProperties({ "summary", "detail", "errors" })
	ApiGatewayMultipleResponse(Forecast[] summary, Map<String, Forecast[]> detail,
			Map<String, ClientServiceErrorResponse> errors) {
		this.summary = summary;
		this.detail = detail;
		this.errors = errors;
	}

	public static ApiGatewayMultipleResponseBuilder builder() {
		return new ApiGatewayMultipleResponseBuilder();
	}

	public Forecast[] getSummary() {
		return this.summary;
	}

	public Map<String, Forecast[]> getDetail() {
		return this.detail;
	}

	public Map<String, ClientServiceErrorResponse> getErrors() {
		return this.errors;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ApiGatewayMultipleResponse)) {
			return false;
		}
		final ApiGatewayMultipleResponse other = (ApiGatewayMultipleResponse) o;
		if (!other.canEqual((Object) this)) {
			return false;
		}
		if (!java.util.Arrays.deepEquals(this.getSummary(), other.getSummary())) {
			return false;
		}
		final Object this$detail = this.getDetail();
		final Object other$detail = other.getDetail();
		if (this$detail == null
				? other$detail != null
				: !this$detail.equals(other$detail)) {
			return false;
		}
		final Object this$errors = this.getErrors();
		final Object other$errors = other.getErrors();
		if (this$errors == null
				? other$errors != null
				: !this$errors.equals(other$errors)) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		result = result * PRIME + java.util.Arrays.deepHashCode(this.getSummary());
		final Object $detail = this.getDetail();
		result = result * PRIME +
				($detail == null
						? 43
						: $detail.hashCode());
		final Object $errors = this.getErrors();
		result = result * PRIME +
				($errors == null
						? 43
						: $errors.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof ApiGatewayMultipleResponse;
	}

	public String toString() {
		return "com.epam.vasyatka.weather.gateway.model.ApiGatewayMultipleResponse(summary=" +
				java.util.Arrays.deepToString(this.getSummary()) +
				", detail=" +
				this.getDetail() +
				", errors=" +
				this.getErrors() +
				")";
	}

	public static class ApiGatewayMultipleResponseBuilder {

		private Forecast[] summary;

		private Map<String, Forecast[]> detail;

		private Map<String, ClientServiceErrorResponse> errors;

		ApiGatewayMultipleResponseBuilder() {
		}

		public ApiGatewayMultipleResponse.ApiGatewayMultipleResponseBuilder summary(Forecast[] summary) {
			this.summary = summary;
			return this;
		}

		public ApiGatewayMultipleResponse.ApiGatewayMultipleResponseBuilder detail(Map<String, Forecast[]> detail) {
			this.detail = detail;
			return this;
		}

		public ApiGatewayMultipleResponse.ApiGatewayMultipleResponseBuilder errors(
				Map<String, ClientServiceErrorResponse> errors) {
			this.errors = errors;
			return this;
		}

		public ApiGatewayMultipleResponse build() {
			return new ApiGatewayMultipleResponse(summary, detail, errors);
		}

		public String toString() {
			return
					"com.epam.vasyatka.weather.gateway.model.ApiGatewayMultipleResponse.ApiGatewayMultipleResponseBuilder(summary=" +
							java.util.Arrays.deepToString(this.summary) +
							", detail=" +
							this.detail +
							", errors=" +
							this.errors +
							")";
		}
	}
}
