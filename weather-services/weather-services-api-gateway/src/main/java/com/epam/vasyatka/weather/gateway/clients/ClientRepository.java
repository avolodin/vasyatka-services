package com.epam.vasyatka.weather.gateway.clients;

import java.util.List;

public interface ClientRepository {

	List<String> getServiceIdentifiers();
}
