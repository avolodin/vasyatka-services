package com.epam.vasyatka.weather.gateway;

import com.epam.vasyatka.weather.gateway.model.ApiGatewayMultipleResponse;
import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import com.epam.vasyatka.weather.integration.model.Forecast;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/")
public class WeatherGatewayRestController {

	private final ClientCaller caller;

	public WeatherGatewayRestController(ClientCaller caller) {
		this.caller = caller;
	}

	@RequestMapping("/{location}/today")
	public ResponseEntity<ApiGatewayMultipleResponse> today(@PathVariable String location) {
		String endpoint = String.format("/%s/today", location);
		Pair<Map<String, Forecast[]>, Map<String, ClientServiceErrorResponse>> response =
				caller.call(endpoint, Forecast[].class);
		Map<String, Forecast[]> details = response.getLeft();
		Map<String, ClientServiceErrorResponse> errors = response.getRight();

		if (details.isEmpty()) {
			return ResponseEntity.badRequest().body(ApiGatewayMultipleResponse
					.builder()
					.errors(errors)
					.build());
		}
		Forecast[] summary = details.get("meteo-info-weather-client");
		return ResponseEntity.ok(ApiGatewayMultipleResponse.builder()
				.summary(summary)
				.detail(details)
				.errors(errors)
				.build());
	}

	@RequestMapping("/{location}/week")
	public ResponseEntity<ApiGatewayMultipleResponse> week(@PathVariable String location) {
		String endpoint = String.format("/%s/week", location);
		Pair<Map<String, Forecast[]>, Map<String, ClientServiceErrorResponse>> response =
				caller.call(endpoint, Forecast[].class);
		Map<String, Forecast[]> details = response.getLeft();
		Map<String, ClientServiceErrorResponse> errors = response.getRight();
		if (details.isEmpty()) {
			return ResponseEntity.badRequest().body(ApiGatewayMultipleResponse
					.builder()
					.errors(errors)
					.build());
		}
		Forecast[] summary = details.get("meteo-info-weather-client");
		return ResponseEntity.ok(ApiGatewayMultipleResponse.builder()
				.summary(summary)
				.detail(details)
				.errors(errors)
				.build());
	}
}