package com.epam.vasyatka.weather.gateway.model;

import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import com.epam.vasyatka.weather.integration.model.Forecast;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiGatewayResponse {

	private final Forecast summary;

	private final Map<String, Forecast> detail;

	private final Map<String, ClientServiceErrorResponse> errors;

	@java.beans.ConstructorProperties({ "summary", "detail", "errors" })
	ApiGatewayResponse(Forecast summary, Map<String, Forecast> detail,
			Map<String, ClientServiceErrorResponse> errors) {
		this.summary = summary;
		this.detail = detail;
		this.errors = errors;
	}

	public static ApiGatewayResponseBuilder builder() {
		return new ApiGatewayResponseBuilder();
	}

	public Forecast getSummary() {
		return this.summary;
	}

	public Map<String, Forecast> getDetail() {
		return this.detail;
	}

	public Map<String, ClientServiceErrorResponse> getErrors() {
		return this.errors;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ApiGatewayResponse)) {
			return false;
		}
		final ApiGatewayResponse other = (ApiGatewayResponse) o;
		if (!other.canEqual((Object) this)) {
			return false;
		}
		final Object this$summary = this.getSummary();
		final Object other$summary = other.getSummary();
		if (this$summary == null
				? other$summary != null
				: !this$summary.equals(other$summary)) {
			return false;
		}
		final Object this$detail = this.getDetail();
		final Object other$detail = other.getDetail();
		if (this$detail == null
				? other$detail != null
				: !this$detail.equals(other$detail)) {
			return false;
		}
		final Object this$errors = this.getErrors();
		final Object other$errors = other.getErrors();
		if (this$errors == null
				? other$errors != null
				: !this$errors.equals(other$errors)) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $summary = this.getSummary();
		result = result * PRIME +
				($summary == null
						? 43
						: $summary.hashCode());
		final Object $detail = this.getDetail();
		result = result * PRIME +
				($detail == null
						? 43
						: $detail.hashCode());
		final Object $errors = this.getErrors();
		result = result * PRIME +
				($errors == null
						? 43
						: $errors.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof ApiGatewayResponse;
	}

	public String toString() {
		return "com.epam.vasyatka.weather.gateway.model.ApiGatewayResponse(summary=" +
				this.getSummary() +
				", detail=" +
				this.getDetail() +
				", errors=" +
				this.getErrors() +
				")";
	}

	public static class ApiGatewayResponseBuilder {

		private Forecast summary;

		private Map<String, Forecast> detail;

		private Map<String, ClientServiceErrorResponse> errors;

		ApiGatewayResponseBuilder() {
		}

		public ApiGatewayResponse.ApiGatewayResponseBuilder summary(Forecast summary) {
			this.summary = summary;
			return this;
		}

		public ApiGatewayResponse.ApiGatewayResponseBuilder detail(Map<String, Forecast> detail) {
			this.detail = detail;
			return this;
		}

		public ApiGatewayResponse.ApiGatewayResponseBuilder errors(Map<String, ClientServiceErrorResponse> errors) {
			this.errors = errors;
			return this;
		}

		public ApiGatewayResponse build() {
			return new ApiGatewayResponse(summary, detail, errors);
		}

		public String toString() {
			return "com.epam.vasyatka.weather.gateway.model.ApiGatewayResponse.ApiGatewayResponseBuilder(summary=" +
					this.summary +
					", detail=" +
					this.detail +
					", errors=" +
					this.errors +
					")";
		}
	}
}
