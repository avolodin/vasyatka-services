# Weather Service
Picture version is available [here](https://docs.google.com/presentation/d/101dzQzsIlYvCcRQ3fQaWnhifed7065WPMrIfUc-N0r4/edit?usp=sharing).

## Request and response examples

All clients, if ```integration-api-spring-boot-starter``` depencency is included, expose two endpoints:
``` GET /{location}/today/ ``` and ``` GET /{location}/week/ ``` which return forecast for a given ```location```
for today or for the current week.

When we call a client service directly, which is by the way what an Api Gateway does, request-response pair
may look like this is case of success:

```
~ $ http :32821/ryazan/today
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Provided-By: meteo-info-client

{
   "date": "2017-11-16T00:00:00Z",
   "precipitationProbability": 87,
   "pressureDay": 750,
   "pressureNight": 749,
   "tempDay": 4,
   "tempNight": -1,
   "windDirection": 240,
   "windSpeed": 6
}

```

and like this is case or client or external system error:
```
~ $ http :32821/moscow/today
HTTP/1.1 400 Bad Request
Content-Type: application/json;charset=UTF-8
Provided-By: meteo-info-client

{
   "description": "org.springframework.dao.EmptyResultDataAccessException: Incorrect result size: expected 1, actual 0",
   "error": "Location moscow is not supported"
}
```

When we call a Gateway, responses from all healthy client services are aggregated:
```
~ $ http :8001/weather/ryazan/today
HTTP/1.1 200 OK
{
   "detail": {
       "meteo-info-client": {
           "date": "2017-11-16T00:00:00Z",
           "precipitationProbability": 87,
           "pressureDay": 750,
           "pressureNight": 749,
           "tempDay": 4,
           "tempNight": -1,
           "windDirection": 240,
           "windSpeed": 6
       }
   },
   "summary": {
       "date": "2017-11-16T00:00:00Z",
       "precipitationProbability": 87,
       "pressureDay": 750,
       "pressureNight": 749,
       "tempDay": 4,
       "tempNight": -1,
       "windDirection": 240,
       "windSpeed": 6
   }
}
```
"Summary" part of the response is hardcoded for now to be an exact copy of meteo-info-client response, but I hope
one day this is gonna change. In future, we want weights for clients to be configurable, so we can trust one client
more that another.

And by the way, gateway is a fine place to employ caching.

In case of client errors, gateway propagates them as well:

```
~ $ http :8001/weather/123/today
HTTP/1.1 400 Bad Request
{
   "errors": {
       "meteo-info-client": {
           "description": "org.springframework.dao.EmptyResultDataAccessException: Incorrect result size: expected 1, actual 0",
           "error": "Location 123 is not supported"
       }
   }
}
```
```
~ $ http :8001/weather/ryazan/today
HTTP/1.1 503 Service Unavailable
Content-Type: application/json;charset=UTF-8
transfer-encoding: chunked

{
    "errors": {
        "gateway": {
            "error": "No services are registered"
        }
    }
}
```

## Client Service in details

Typical Client App to a Weather provider may look like this:

![Client App Structure](../diagrams/client-app-structure.png)


## Integration package
There are two artifacts, ```integration``` and ```integration-api-spring-boot-starter```.
First one contains a couple of interfaces and POJOs that are shared among systems and second one provides a
spring auto-configuration for a client services. If writing a new client, you probably wanna include the latter.

Keep in mind that spring boot and spring cloud versions in both ```integration-api-spring-boot-starter```
and your ```some-new-client``` artifacts should be the same:
```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>this-should-be-the-same</version>
</parent>
<properties>
    <spring-cloud.version>and-this-too</spring-cloud.version>
</properties>
```

At the time of writing this README we are using ```spring-boot-starter: 1.5.9.RELEASE``` and
```spring-cloud: Edgware.RELEASE```.