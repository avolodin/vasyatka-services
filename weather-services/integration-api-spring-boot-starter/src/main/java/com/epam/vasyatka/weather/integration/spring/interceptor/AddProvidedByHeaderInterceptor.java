package com.epam.vasyatka.weather.integration.spring.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component // FIXME: can we make it once per request filter with constructor injection?
public class AddProvidedByHeaderInterceptor extends HandlerInterceptorAdapter {

	@Value("${service.client.identifier_header}")
	private String headerName;

	@Value("${spring.application.name}")
	private String appName;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String header = response.getHeader(headerName);
		if (header == null) {
			response.addHeader(headerName, appName);
		}

		return true;
	}
}
