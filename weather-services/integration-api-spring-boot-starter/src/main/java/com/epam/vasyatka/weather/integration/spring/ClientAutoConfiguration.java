package com.epam.vasyatka.weather.integration.spring;

import com.epam.vasyatka.weather.integration.connector.ProviderConnector;
import com.epam.vasyatka.weather.integration.spring.interceptor.AddProvidedByHeaderInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.time.Duration;

@Configuration
@ComponentScan(basePackageClasses = ClientAutoConfiguration.class)
@ConditionalOnClass(ProviderConnector.class)
public class ClientAutoConfiguration extends WebMvcConfigurerAdapter {

	@Value("${provider.timeout}")
	private int providerCallTimeout;

	private final AddProvidedByHeaderInterceptor addProvidedByHeaderInterceptor;

	public ClientAutoConfiguration(AddProvidedByHeaderInterceptor interceptor) {
		this.addProvidedByHeaderInterceptor = interceptor;
	}

	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(addProvidedByHeaderInterceptor);
	}

	@Bean
	@ConditionalOnMissingBean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		int timeout = (int) Duration.ofSeconds(providerCallTimeout).toMillis();
		RestTemplate build = builder
				.setConnectTimeout(timeout)
				.setReadTimeout(timeout)
				.build();
		return build;
	}
}
