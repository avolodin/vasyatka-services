package com.epam.vasyatka.weather.integration.spring.controller;

import com.epam.vasyatka.weather.integration.model.ClientServiceErrorResponse;
import com.epam.vasyatka.weather.integration.model.exception.LocationNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@ControllerAdvice
public class ClientExceptionHandler {

	@ExceptionHandler(LocationNotSupportedException.class)
	@ResponseStatus(BAD_REQUEST)
	@ResponseBody
	public ClientServiceErrorResponse notSupported(LocationNotSupportedException ex) {
		return ClientServiceErrorResponse.builder()
				.error(String.format("Location %s is not supported", ex.getLocation()))
				.description(ex.getMessage())
				.build();
	}

	@ExceptionHandler({ ResourceAccessException.class, HttpServerErrorException.class })
	@ResponseStatus(SERVICE_UNAVAILABLE)
	@ResponseBody
	public ClientServiceErrorResponse serviceUnavailable(ResourceAccessException ex) {
		return ClientServiceErrorResponse.builder()
				.error("Provider didn't answer")
				.description(ex.getMessage())
				.build();
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ClientServiceErrorResponse serverError(RuntimeException ex) {
		Throwable cause = ex.getCause();
		return ClientServiceErrorResponse.builder()
				.error(ex.getMessage())
				.description(cause == null
						? null
						: cause.toString())
				.build();
	}
}
