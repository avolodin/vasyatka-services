package com.epam.vasyatka.weather.integration.spring.controller;

import com.epam.vasyatka.weather.integration.connector.ProviderConnector;
import com.epam.vasyatka.weather.integration.model.Forecast;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class ClientRestController {

	private final ProviderConnector client;

	public ClientRestController(ProviderConnector client) {
		this.client = client;
	}

	@GetMapping("/{location}/today")
	public List<Forecast> today(@PathVariable String location) {
		return client.today(location);
	}

	@GetMapping("/{location}/week")
	public List<Forecast> week(@PathVariable String location) {
		return client.week(location);
	}
}
