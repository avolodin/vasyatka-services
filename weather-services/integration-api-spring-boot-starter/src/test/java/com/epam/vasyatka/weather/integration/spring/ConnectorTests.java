package com.epam.vasyatka.weather.integration.spring;

import com.epam.vasyatka.weather.integration.connector.ProviderConnector;
import com.epam.vasyatka.weather.integration.model.Forecast;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class ConnectorTests {

	@Autowired
	private ProviderConnector connector;

	@Autowired
	private RestTemplate template;

	private MockRestServiceServer server;

	@Before
	public void setUp() throws Exception {
		server = MockRestServiceServer.bindTo(template).build();
	}

	@Test
	public void handleSuccessResponse() throws Exception {
		server.expect(once(), requestTo(providerEndpoint())).andExpect(method(GET))
				.andRespond(withSuccess().body(successExternalResponse()));
		List<Forecast> responseList = connector.today(location());
		List<Forecast> expectedList = successConnectorResponse();
		assertThat(responseList).isEqualTo(expectedList);
		server.verify();
	}

	@Test
	public void handleMalformedResponse() throws Exception {
		server.expect(once(), requestTo(providerEndpoint())).andExpect(method(GET))
				.andRespond(withSuccess().body(malformedExternalResponse()));
		assertThatThrownBy(() -> connector.today(location())).isInstanceOf(RuntimeException.class)
				.hasMessage("Couldn't convert response from the provider");
		server.verify();
	}

	@Test
	public void handleServerError() throws Exception {
		server.expect(once(), requestTo(providerEndpoint())).andExpect(method(GET)).andRespond(withServerError());
		assertThatThrownBy(() -> connector.today(location())).isExactlyInstanceOf(HttpServerErrorException.class);
		server.verify();
	}

	/*
	 * https://jira.spring.io/browse/SPR-14458 Support timeout settings in
	 * MockRestServiceServer As it is stated in task description,
	 * MockRestServiceServer replaces ClientHttpRequestFactory in the given
	 * RestTemplate so this test is not gonna work as we expect it to.
	 *
	 * What we want from client implementation is to throw a
	 * ResourceAccessException if an external system took too long to respond.
	 */
	// @Test
	// public void handleNoResponse() throws Exception {
	// server.expect(once(),
	// requestTo(Matchers.startsWith(endpoint))).andExpect(method(GET))
	// .andRespond((request) -> {
	// try {
	// Thread.sleep(Duration.ofSeconds(12).toMillis());
	// } catch (InterruptedException ex) {
	// throw new RuntimeException(ex);
	// }
	// return new MockClientHttpResponse(successExternalResponse().getBytes(),
	// HttpStatus.OK);
	// });
	//
	// assertThatThrownBy(
	// () -> connector.today(RYAZAN)
	// ).isExactlyInstanceOf(ResourceAccessException.class);
	// }

	protected abstract String location();

	protected abstract String providerEndpoint();

	protected abstract String successExternalResponse();

	protected abstract List<Forecast> successConnectorResponse();

	protected abstract String malformedExternalResponse();
}
