package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
/**
 * Aka ClientServiceSuccessResponse.
 */
public class Forecast {

	private final Instant date;

	private final String weatherDescription;

	private final TemperatureInfo temperature;

	private final WindInfo wind;

	private final PressureInfo pressure;

	private final PrecipitationInfo precipitation;

	private final HumidityInfo humidity;

	private final CloudsInfo clouds;

	@java.beans.ConstructorProperties({ "date", "weatherDescription", "temperature", "wind", "pressure",
			"precipitation", "humidity", "clouds", "" })
	Forecast(Instant date, String weatherDescription, TemperatureInfo temperature, WindInfo wind,
			PressureInfo pressure, PrecipitationInfo precipitation, HumidityInfo humidity, CloudsInfo clouds) {
		super();
		this.date = date;
		this.weatherDescription = weatherDescription;
		this.temperature = temperature;
		this.wind = wind;
		this.pressure = pressure;
		this.precipitation = precipitation;
		this.humidity = humidity;
		this.clouds = clouds;
	}

	public final String getWeatherDescription() {
		return weatherDescription;
	}

	public final Instant getDate() {
		return date;
	}

	public final TemperatureInfo getTemperature() {
		return temperature;
	}

	public final WindInfo getWind() {
		return wind;
	}

	public final PressureInfo getPressure() {
		return pressure;
	}

	public final PrecipitationInfo getPrecipitation() {
		return precipitation;
	}

	public final HumidityInfo getHumidity() {
		return humidity;
	}

	public final CloudsInfo getClouds() {
		return clouds;
	}

	public static ForecastBuilder builder() {
		return new ForecastBuilder();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clouds == null) ? 0 : clouds.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((humidity == null) ? 0 : humidity.hashCode());
		result = prime * result + ((precipitation == null) ? 0 : precipitation.hashCode());
		result = prime * result + ((pressure == null) ? 0 : pressure.hashCode());
		result = prime * result + ((temperature == null) ? 0 : temperature.hashCode());
		result = prime * result + ((weatherDescription == null) ? 0 : weatherDescription.hashCode());
		result = prime * result + ((wind == null) ? 0 : wind.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Forecast other = (Forecast) obj;
		if (clouds == null) {
			if (other.clouds != null)
				return false;
		} else if (!clouds.equals(other.clouds))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (humidity == null) {
			if (other.humidity != null)
				return false;
		} else if (!humidity.equals(other.humidity))
			return false;
		if (precipitation == null) {
			if (other.precipitation != null)
				return false;
		} else if (!precipitation.equals(other.precipitation))
			return false;
		if (pressure == null) {
			if (other.pressure != null)
				return false;
		} else if (!pressure.equals(other.pressure))
			return false;
		if (temperature == null) {
			if (other.temperature != null)
				return false;
		} else if (!temperature.equals(other.temperature))
			return false;
		if (weatherDescription == null) {
			if (other.weatherDescription != null)
				return false;
		} else if (!weatherDescription.equals(other.weatherDescription))
			return false;
		if (wind == null) {
			if (other.wind != null)
				return false;
		} else if (!wind.equals(other.wind))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.integration.model.Forecast (date=" + date + ", weatherDescription="
				+ weatherDescription + ", temperature=" + temperature + ", wind=" + wind + ", pressure=" + pressure
				+ ", precipitation=" + precipitation + ", humidity=" + humidity + ", clouds=" + clouds + ")";
	}

	public static class ForecastBuilder {
		private Instant date;

		private String weatherDescription;

		private TemperatureInfo temperature;

		private WindInfo wind;

		private PressureInfo pressure;

		private PrecipitationInfo precipitation;

		private HumidityInfo humidity;

		private CloudsInfo clouds;

		public Forecast.ForecastBuilder date(Instant date) {
			this.date = date;
			return this;
		}

		public Forecast.ForecastBuilder weatherDescription(String weatherDescription) {
			this.weatherDescription = weatherDescription;
			return this;
		}

		public Forecast.ForecastBuilder temperature(TemperatureInfo temperature) {
			this.temperature = temperature;
			return this;
		}

		public Forecast.ForecastBuilder wind(WindInfo wind) {
			this.wind = wind;
			return this;
		}

		public Forecast.ForecastBuilder pressure(PressureInfo pressure) {
			this.pressure = pressure;
			return this;
		}

		public Forecast.ForecastBuilder precipitation(PrecipitationInfo precipitation) {
			this.precipitation = precipitation;
			return this;
		}

		public Forecast.ForecastBuilder humidity(HumidityInfo humidity) {
			this.humidity = humidity;
			return this;
		}

		public Forecast.ForecastBuilder clouds(CloudsInfo clouds) {
			this.clouds = clouds;
			return this;
		}

		public Forecast build() {
			return new Forecast(date, weatherDescription, temperature, wind, pressure, precipitation, humidity, clouds);
		}
	}
}
