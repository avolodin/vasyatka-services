package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WindInfo {
	private final Float minSpeed;
	private final Float maxSpeed;
	private final String direction;
	private final String unit;

	public final Float getMinSpeed() {
		return minSpeed;
	}

	public final Float getMaxSpeed() {
		return maxSpeed;
	}

	public final String getDirection() {
		return direction;
	}

	public final String getUnit() {
		return unit;
	}

	@java.beans.ConstructorProperties({ "minSpeed", "maxSpeed", "direction", "unit" })
	public WindInfo(Float minSpeed, Float maxSpeed, String direction, String unit) {
		super();
		this.minSpeed = minSpeed;
		this.maxSpeed = maxSpeed;
		this.direction = direction;
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		result = prime * result + ((maxSpeed == null) ? 0 : maxSpeed.hashCode());
		result = prime * result + ((minSpeed == null) ? 0 : minSpeed.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WindInfo other = (WindInfo) obj;
		if (direction == null) {
			if (other.direction != null)
				return false;
		} else if (!direction.equals(other.direction))
			return false;
		if (maxSpeed == null) {
			if (other.maxSpeed != null)
				return false;
		} else if (!maxSpeed.equals(other.maxSpeed))
			return false;
		if (minSpeed == null) {
			if (other.minSpeed != null)
				return false;
		} else if (!minSpeed.equals(other.minSpeed))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.integration.model.WindInfo (minSpeed=" + minSpeed + ", maxSpeed=" + maxSpeed
				+ ", direction=" + direction + ", unit=" + unit + ")";
	}

}
