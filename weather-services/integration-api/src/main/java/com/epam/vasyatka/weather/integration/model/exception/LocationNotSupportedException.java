package com.epam.vasyatka.weather.integration.model.exception;

public class LocationNotSupportedException extends RuntimeException {

	private final String location;

	public LocationNotSupportedException(String location, Throwable cause) {
		super(cause);
		this.location = location;
	}

	public String getLocation() {
		return this.location;
	}
}
