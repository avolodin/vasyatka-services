package com.epam.vasyatka.weather.integration.connector;

/**
 * Maps user input to it's representation in the given external system.
 * <p>
 * For example: meteoinfo provider can return "27730" for id(RYAZAN), whether another service has different id for it.
 */
public interface ProviderLocationMapper {

	String id(String location);
}
