package com.epam.vasyatka.weather.integration.connector;

import com.epam.vasyatka.weather.integration.model.Forecast;

import java.util.List;

/**
 * Public API which Client to the Weather Provider (e.g. meteoinfo) should expose.
 */
public interface ProviderConnector {

	/**
	 * Returns forecast for today.
	 */
	List<Forecast> today(String location);

	/**
	 * Return forecast for one week, starting from today. For now, let's ensure entries are sorted by date asc.
	 */
	List<Forecast> week(String location);
}
