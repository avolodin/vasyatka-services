package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HumidityInfo {
	private final Integer value;
	private final String unit;

	public final Integer getValue() {
		return value;
	}

	public final String getUnit() {
		return unit;
	}

	@java.beans.ConstructorProperties({ "value", "unit" })
	public HumidityInfo(Integer value, String unit) {
		super();
		this.value = value;
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HumidityInfo other = (HumidityInfo) obj;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.integration.model.HumidityInfo (value=" + value + ", unit=" + unit + ")";
	}

}
