package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CloudsInfo {
	private final Integer value;
	private final String unit;
	private final String description;

	public final Integer getValue() {
		return value;
	}

	public final String getUnit() {
		return unit;
	}

	public final String getDescription() {
		return description;
	}

	@java.beans.ConstructorProperties({ "value", "unit", "description" })
	public CloudsInfo(Integer value, String unit, String description) {
		super();
		this.value = value;
		this.unit = unit;
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CloudsInfo other = (CloudsInfo) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.integration.model.CloudsInfo (value=" + value + ", unit=" + unit
				+ ", description=" + description + ")";
	}

}
