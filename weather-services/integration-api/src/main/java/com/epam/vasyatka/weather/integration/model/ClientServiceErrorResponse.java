package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
/**
 * Returned by Provider Client if something went wrong.
 */
public class ClientServiceErrorResponse {

	private final String error;

	private final String description;

	@java.beans.ConstructorProperties({ "error", "description" })
	ClientServiceErrorResponse(String error, String description) {
		this.error = error;
		this.description = description;
	}

	public static ClientServiceErrorResponseBuilder builder() {
		return new ClientServiceErrorResponseBuilder();
	}

	public String getError() {
		return this.error;
	}

	public String getDescription() {
		return this.description;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ClientServiceErrorResponse)) {
			return false;
		}
		final ClientServiceErrorResponse other = (ClientServiceErrorResponse) o;
		if (!other.canEqual((Object) this)) {
			return false;
		}
		final Object this$error = this.getError();
		final Object other$error = other.getError();
		if (this$error == null
				? other$error != null
				: !this$error.equals(other$error)) {
			return false;
		}
		final Object this$description = this.getDescription();
		final Object other$description = other.getDescription();
		if (this$description == null
				? other$description != null
				: !this$description.equals(other$description)) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $error = this.getError();
		result = result * PRIME +
				($error == null
						? 43
						: $error.hashCode());
		final Object $description = this.getDescription();
		result = result * PRIME +
				($description == null
						? 43
						: $description.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof ClientServiceErrorResponse;
	}

	public String toString() {
		return "com.epam.magicweather.integration.model.ClientServiceErrorResponse(error=" +
				this.getError() +
				", description=" +
				this.getDescription() +
				")";
	}

	public static class ClientServiceErrorResponseBuilder {

		private String error;

		private String description;

		ClientServiceErrorResponseBuilder() {
		}

		public ClientServiceErrorResponse.ClientServiceErrorResponseBuilder error(String error) {
			this.error = error;
			return this;
		}

		public ClientServiceErrorResponse.ClientServiceErrorResponseBuilder description(String description) {
			this.description = description;
			return this;
		}

		public ClientServiceErrorResponse build() {
			return new ClientServiceErrorResponse(error, description);
		}

		public String toString() {
			return
					"com.epam.magicweather.integration.model.ClientServiceErrorResponse.ClientServiceErrorResponseBuilder(error=" +
							this.error +
							", description=" +
							this.description +
							")";
		}
	}
}
