package com.epam.vasyatka.weather.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PrecipitationInfo {
	private final Float value;
	private final String unit;
	private final String type;
	private final String intencity;
	private final Integer probability;

	public final Float getValue() {
		return value;
	}

	public final String getUnit() {
		return unit;
	}

	public final String getType() {
		return type;
	}

	public final String getIntencity() {
		return intencity;
	}

	public final Integer getProbability() {
		return probability;
	}

	@java.beans.ConstructorProperties({ "value", "unit", "type", "intencity", "probability" })
	public PrecipitationInfo(Float value, String unit, String type, String intencity, Integer probability) {
		super();
		this.value = value;
		this.unit = unit;
		this.type = type;
		this.intencity = intencity;
		this.probability = probability;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((intencity == null) ? 0 : intencity.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrecipitationInfo other = (PrecipitationInfo) obj;
		if (intencity == null) {
			if (other.intencity != null)
				return false;
		} else if (!intencity.equals(other.intencity))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "com.epam.vasyatka.weather.integration.model.PrecipitationInfo (value=" + value + ", unit=" + unit
				+ ", type=" + type + ", intencity=" + intencity + ")";
	}

}
