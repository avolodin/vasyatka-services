package com.epam.vasyatka.weather.client.meteoinfo;

import com.epam.vasyatka.weather.integration.model.CloudsInfo;
import com.epam.vasyatka.weather.integration.model.Forecast;
import com.epam.vasyatka.weather.integration.model.HumidityInfo;
import com.epam.vasyatka.weather.integration.model.PrecipitationInfo;
import com.epam.vasyatka.weather.integration.model.PressureInfo;
import com.epam.vasyatka.weather.integration.model.TemperatureInfo;
import com.epam.vasyatka.weather.integration.model.WindInfo;
import com.epam.vasyatka.weather.integration.spring.ConnectorTests;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class MeteoInfoConnectorTests extends ConnectorTests {

	@Override
	protected String location() {
		return "ryazan";
	}

	@Override
	protected String providerEndpoint() {
		return "http://meteoinfo.ru/rppr.php?s=27730";
	}

	@Override
	protected String successExternalResponse() {
		return "MjAxNy0xMS0wMjstNTsxOzY7MjUwOzM7MjI7NzM3Ozc0MDsyMDE3LTExLTAzOy00OzE7MTQ7MTMwOzI7Nj"
				+ "I7NzQwOzc0NDsyMDE3LTExLTA0Oy0xOzI7NjsyOTA7MjszNDs3NDQ7NzUyOzIwMTctMTEtMDU7LTE7Mzs2OzIxMDsyOzE3Ozc1MTs3NTU7Mj"
				+ "AxNy0xMS0wNjstMTs1OzY7MTcwOzM7Mjc7NzU1Ozc1NzsyMDE3LTExLTA3OzI7Nzs2OzI0MDsyOzIzOzc1Njs3NTk7MjAxNy0xMS0wODsxOz"
				+ "Q7NjszNTA7MjsxODs3NTg7NzYxOzIwMTctMTEtMDk7LTE7MjsxNDsxMjA7Mjs1NTs3NjI7NzU5Ow==";
	}

	@Override
	protected List<Forecast> successConnectorResponse() {
		float averageTemperature = -2;
		float averagePressure = 738.5f;
		TemperatureInfo temperature = new TemperatureInfo(averageTemperature, null, "celsius");
		WindInfo windInfo = new WindInfo((float) 3, null, null, "mps");
		PressureInfo pressureInfo = new PressureInfo(averagePressure, null, "mmHg");
		PrecipitationInfo precipitationInfo = null;
		HumidityInfo humidityInfo = null;
		CloudsInfo cloudsInfo = null;
		List<Forecast> successResponse = new ArrayList<>();
		Forecast responseElement =  Forecast.builder().date(Instant.parse("2017-11-02T00:00:00Z")).weatherDescription(null)
				.temperature(temperature).wind(windInfo).pressure(pressureInfo).precipitation(precipitationInfo)
				.humidity(humidityInfo).clouds(cloudsInfo).build();
		successResponse.add(responseElement);
		return successResponse;
	}

	@Override
	protected String malformedExternalResponse() {
		return "MjAxNy0xMS0wMjstNTsxOzY7MjUwOzM7MjI7NzM3Ozc0MDsyMDE3LTExLTAzOy00OzE7MTQ7MTMwOzI7Nj"
				+ "I7NzQwOzc0NDsyMDE3LTExLTA0Oy0xOzI7NjsyOTA7MjszNDs3NDQ7NzUyOzIwMTctMTEtMDU7LTE7Mzs2OzIxMDsyOzE3Ozc1MTs3NTU7Mj"
				+ "AxNy0xMS0wNjstMTs1OzY7MTcwOzM7Mjc7NzU1Ozc1NzsyMDE3LTExLTA3OzI7Nzs2OzI0MDsyOzIzOzc1Njs3NTk7MjAxNy0xMS0wODsxOz"
				+ "FFFFFFFFF7MjsxODs3NTg7NzYxOzIwMTctMTEtMDk7LTE7MjsxNDsxMjA7Mjs1NTs3NjI7NzU5Ow==";
	}
}
