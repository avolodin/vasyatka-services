package com.epam.vasyatka.weather.client.meteoinfo.connector.converter;

public class MeteoInfoResponse {

	private final String date;

	private final Integer tempNight;

	private final Integer tempDay;

	private final Integer commentId;

	private final Integer windDirection;

	private final Integer windSpeed;

	private final Integer precipitationProbability;

	private final Integer pressureNight;

	private final Integer pressureDay;

	@java.beans.ConstructorProperties({ "date",
			"tempNight",
			"tempDay",
			"commentId",
			"windDirection",
			"windSpeed",
			"precipitationProbability",
			"pressureNight",
			"pressureDay" })
	MeteoInfoResponse(String date, Integer tempNight, Integer tempDay, Integer commentId, Integer windDirection,
			Integer windSpeed, Integer precipitationProbability, Integer pressureNight, Integer pressureDay) {
		this.date = date;
		this.tempNight = tempNight;
		this.tempDay = tempDay;
		this.commentId = commentId;
		this.windDirection = windDirection;
		this.windSpeed = windSpeed;
		this.precipitationProbability = precipitationProbability;
		this.pressureNight = pressureNight;
		this.pressureDay = pressureDay;
	}

	public static MeteoInfoResponseBuilder builder() {
		return new MeteoInfoResponseBuilder();
	}

	public String getDate() {
		return this.date;
	}

	public Integer getTempNight() {
		return this.tempNight;
	}

	public Integer getTempDay() {
		return this.tempDay;
	}

	public Integer getCommentId() {
		return this.commentId;
	}

	public Integer getWindDirection() {
		return this.windDirection;
	}

	public Integer getWindSpeed() {
		return this.windSpeed;
	}

	public Integer getPrecipitationProbability() {
		return this.precipitationProbability;
	}

	public Integer getPressureNight() {
		return this.pressureNight;
	}

	public Integer getPressureDay() {
		return this.pressureDay;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof MeteoInfoResponse)) {
			return false;
		}
		final MeteoInfoResponse other = (MeteoInfoResponse) o;
		if (!other.canEqual((Object) this)) {
			return false;
		}
		final Object this$date = this.getDate();
		final Object other$date = other.getDate();
		if (this$date == null
				? other$date != null
				: !this$date.equals(other$date)) {
			return false;
		}
		final Object this$tempNight = this.getTempNight();
		final Object other$tempNight = other.getTempNight();
		if (this$tempNight == null
				? other$tempNight != null
				: !this$tempNight.equals(other$tempNight)) {
			return false;
		}
		final Object this$tempDay = this.getTempDay();
		final Object other$tempDay = other.getTempDay();
		if (this$tempDay == null
				? other$tempDay != null
				: !this$tempDay.equals(other$tempDay)) {
			return false;
		}
		final Object this$commentId = this.getCommentId();
		final Object other$commentId = other.getCommentId();
		if (this$commentId == null
				? other$commentId != null
				: !this$commentId.equals(other$commentId)) {
			return false;
		}
		final Object this$windDirection = this.getWindDirection();
		final Object other$windDirection = other.getWindDirection();
		if (this$windDirection == null
				? other$windDirection != null
				: !this$windDirection.equals(other$windDirection)) {
			return false;
		}
		final Object this$windSpeed = this.getWindSpeed();
		final Object other$windSpeed = other.getWindSpeed();
		if (this$windSpeed == null
				? other$windSpeed != null
				: !this$windSpeed.equals(other$windSpeed)) {
			return false;
		}
		final Object this$precipitationProbability = this.getPrecipitationProbability();
		final Object other$precipitationProbability = other.getPrecipitationProbability();
		if (this$precipitationProbability == null
				? other$precipitationProbability != null
				: !this$precipitationProbability.equals(other$precipitationProbability)) {
			return false;
		}
		final Object this$pressureNight = this.getPressureNight();
		final Object other$pressureNight = other.getPressureNight();
		if (this$pressureNight == null
				? other$pressureNight != null
				: !this$pressureNight.equals(other$pressureNight)) {
			return false;
		}
		final Object this$pressureDay = this.getPressureDay();
		final Object other$pressureDay = other.getPressureDay();
		if (this$pressureDay == null
				? other$pressureDay != null
				: !this$pressureDay.equals(other$pressureDay)) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final Object $date = this.getDate();
		result = result * PRIME +
				($date == null
						? 43
						: $date.hashCode());
		final Object $tempNight = this.getTempNight();
		result = result * PRIME +
				($tempNight == null
						? 43
						: $tempNight.hashCode());
		final Object $tempDay = this.getTempDay();
		result = result * PRIME +
				($tempDay == null
						? 43
						: $tempDay.hashCode());
		final Object $commentId = this.getCommentId();
		result = result * PRIME +
				($commentId == null
						? 43
						: $commentId.hashCode());
		final Object $windDirection = this.getWindDirection();
		result = result * PRIME +
				($windDirection == null
						? 43
						: $windDirection.hashCode());
		final Object $windSpeed = this.getWindSpeed();
		result = result * PRIME +
				($windSpeed == null
						? 43
						: $windSpeed.hashCode());
		final Object $precipitationProbability = this.getPrecipitationProbability();
		result = result * PRIME +
				($precipitationProbability == null
						? 43
						: $precipitationProbability.hashCode());
		final Object $pressureNight = this.getPressureNight();
		result = result * PRIME +
				($pressureNight == null
						? 43
						: $pressureNight.hashCode());
		final Object $pressureDay = this.getPressureDay();
		result = result * PRIME +
				($pressureDay == null
						? 43
						: $pressureDay.hashCode());
		return result;
	}

	protected boolean canEqual(Object other) {
		return other instanceof MeteoInfoResponse;
	}

	public String toString() {
		return "com.epam.magicweather.client.meteoinfo.connector.converter.MeteoInfoResponse(date=" +
				this.getDate() +
				", tempNight=" +
				this.getTempNight() +
				", tempDay=" +
				this.getTempDay() +
				", commentId=" +
				this.getCommentId() +
				", windDirection=" +
				this.getWindDirection() +
				", windSpeed=" +
				this.getWindSpeed() +
				", precipitationProbability=" +
				this.getPrecipitationProbability() +
				", pressureNight=" +
				this.getPressureNight() +
				", pressureDay=" +
				this.getPressureDay() +
				")";
	}

	public static class MeteoInfoResponseBuilder {

		private String date;

		private Integer tempNight;

		private Integer tempDay;

		private Integer commentId;

		private Integer windDirection;

		private Integer windSpeed;

		private Integer precipitationProbability;

		private Integer pressureNight;

		private Integer pressureDay;

		MeteoInfoResponseBuilder() {
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder date(String date) {
			this.date = date;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder tempNight(Integer tempNight) {
			this.tempNight = tempNight;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder tempDay(Integer tempDay) {
			this.tempDay = tempDay;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder commentId(Integer commentId) {
			this.commentId = commentId;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder windDirection(Integer windDirection) {
			this.windDirection = windDirection;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder windSpeed(Integer windSpeed) {
			this.windSpeed = windSpeed;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder precipitationProbability(Integer precipitationProbability) {
			this.precipitationProbability = precipitationProbability;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder pressureNight(Integer pressureNight) {
			this.pressureNight = pressureNight;
			return this;
		}

		public MeteoInfoResponse.MeteoInfoResponseBuilder pressureDay(Integer pressureDay) {
			this.pressureDay = pressureDay;
			return this;
		}

		public MeteoInfoResponse build() {
			return new MeteoInfoResponse(date, tempNight, tempDay, commentId, windDirection, windSpeed,
					precipitationProbability, pressureNight, pressureDay);
		}

		public String toString() {
			return
					"com.epam.magicweather.client.meteoinfo.connector.converter.MeteoInfoResponse.MeteoInfoResponseBuilder(date=" +
							this.date +
							", tempNight=" +
							this.tempNight +
							", tempDay=" +
							this.tempDay +
							", commentId=" +
							this.commentId +
							", windDirection=" +
							this.windDirection +
							", windSpeed=" +
							this.windSpeed +
							", precipitationProbability=" +
							this.precipitationProbability +
							", pressureNight=" +
							this.pressureNight +
							", pressureDay=" +
							this.pressureDay +
							")";
		}
	}
}
