package com.epam.vasyatka.weather.client.meteoinfo.connector.converter;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CommentResolver {
	private static final String FEW_CLOUDS_DESCRIPTION = "Few clouds";
	private static final String CLOUDY_DESCRIPTION = "Cloudy";
	private static final String PARTLY_CLOUDY_DESCRIPTION = "Partly Cloudy";
	private static final String THUNDER_DESCRIPTION = "Rain with Thunder";
	private static final String LIGHT_RAIN_DESCRIPTION = "Light Rain";
	private static final String RAIN_DESCRIPTION = "Rain";
	private static final String LIGHT_SNOW_DESCRIPTION = "Light Snow";
	private static final String PRECIPITATION_DESCRIPTION = "Precipitation";
	private static final String SMALL_PRECIPITATION_DESCRIPTION = "Small Precipitation";
	private static final String SNOW_DESCRIPTION = "Snow";
	private static final String HAIL_DESCRIPTION = "Possible Hail";
	private static final String NO_PRECIPITATION_DESCRIPTION = "No Precipitation";
	private static final String BLIZZARD_DESCRIPTION = "Blizzard";

	private final Map<Integer, String> commentById = new HashMap<Integer, String>() {
		{
			put(1, String.format("%s, %s", CLOUDY_DESCRIPTION, RAIN_DESCRIPTION));
			put(2, String.format("%s, %s", CLOUDY_DESCRIPTION, SNOW_DESCRIPTION));
			put(3, String.format("%s, %s", CLOUDY_DESCRIPTION, HAIL_DESCRIPTION));
			put(4, String.format("%s, %s", CLOUDY_DESCRIPTION, PRECIPITATION_DESCRIPTION));
			put(5, String.format("%s, %s", CLOUDY_DESCRIPTION, NO_PRECIPITATION_DESCRIPTION));
			put(6, String.format("%s, %s", PARTLY_CLOUDY_DESCRIPTION, NO_PRECIPITATION_DESCRIPTION));
			put(7, String.format("%s, %s", FEW_CLOUDS_DESCRIPTION, NO_PRECIPITATION_DESCRIPTION));
			put(8, String.format("%s, %s", CLOUDY_DESCRIPTION, THUNDER_DESCRIPTION));
			put(9, String.format("%s, %s", PARTLY_CLOUDY_DESCRIPTION, RAIN_DESCRIPTION));
			put(10, String.format("%s, %s", PARTLY_CLOUDY_DESCRIPTION, LIGHT_RAIN_DESCRIPTION));
			put(11, String.format("%s, %s", CLOUDY_DESCRIPTION, LIGHT_RAIN_DESCRIPTION));
			put(12, String.format("%s, %s", PARTLY_CLOUDY_DESCRIPTION, LIGHT_SNOW_DESCRIPTION));
			put(13, String.format("%s, %s", CLOUDY_DESCRIPTION, LIGHT_SNOW_DESCRIPTION));
			put(14, String.format("%s, %s", PARTLY_CLOUDY_DESCRIPTION, SMALL_PRECIPITATION_DESCRIPTION));
			put(15, String.format("%s, %s", CLOUDY_DESCRIPTION, SMALL_PRECIPITATION_DESCRIPTION));
			put(16, String.format("%s, %s", CLOUDY_DESCRIPTION, NO_PRECIPITATION_DESCRIPTION));
			put(17, BLIZZARD_DESCRIPTION);
		}
	};

	public String byId(Integer id) {
		return commentById.get(id);
	}
}
