package com.epam.vasyatka.weather.client.meteoinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeteoInfoWeatherClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeteoInfoWeatherClientApplication.class, args);
	}
}
