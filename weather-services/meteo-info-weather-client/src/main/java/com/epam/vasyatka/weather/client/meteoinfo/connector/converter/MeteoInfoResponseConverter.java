package com.epam.vasyatka.weather.client.meteoinfo.connector.converter;

import com.epam.vasyatka.weather.integration.model.Forecast;
import com.epam.vasyatka.weather.integration.model.CloudsInfo;
import com.epam.vasyatka.weather.integration.model.TemperatureInfo;
import com.epam.vasyatka.weather.integration.model.HumidityInfo;
import com.epam.vasyatka.weather.integration.model.WindInfo;
import com.epam.vasyatka.weather.integration.model.PressureInfo;
import com.epam.vasyatka.weather.integration.model.PrecipitationInfo;

import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MeteoInfoResponseConverter {

	private final CommentResolver commentResolver;

	public MeteoInfoResponseConverter(CommentResolver commentResolver) {
		this.commentResolver = commentResolver;
	}

	public List<Forecast> today(String response) {
		try {
			List<MeteoInfoResponse> rawResponse = call(response);
			MeteoInfoResponse todayRaw = rawResponse.get(0);
			return Arrays.asList(convert(todayRaw));
		} catch (RuntimeException ex) {
			throw new RuntimeException("Couldn't convert response from the provider", ex);
		}
	}

	private List<MeteoInfoResponse> call(String response) {
		byte[] bytes = Base64Utils.decodeFromString(response);
		String[] parts = new String(bytes).split(";");
		int elementsPerEntry = 9;
		int entryCount = parts.length / elementsPerEntry;
		if (entryCount < 1) {
			throw new RuntimeException("Malformed response: " + response);
		}

		List<MeteoInfoResponse> rawResponse = new ArrayList<>();
		for (int i = 0; i < parts.length; i += elementsPerEntry) {
			String[] e = Arrays.copyOfRange(parts, i, i + elementsPerEntry);
			MeteoInfoResponse meteoInfoResponse = MeteoInfoResponse.builder().date(e[0])
					.tempNight(Integer.valueOf(e[1]))
					.tempDay(Integer.valueOf(e[2]))
					.commentId(Integer.valueOf(e[3]))
					.windDirection(Integer.valueOf(e[4]))
					.windSpeed(Integer.valueOf(e[5]))
					.precipitationProbability(Integer.valueOf(e[6]))
					.pressureNight(Integer.valueOf(e[7]))
					.pressureDay(Integer.valueOf(e[8])).build();
			rawResponse.add(meteoInfoResponse);
		}
		return rawResponse;
	}

	private Forecast convert(MeteoInfoResponse response) {
		float averageTemperature = (((float) response.getTempDay() + response.getTempNight()) / 2);
		float averagePressure = ((float) (response.getPressureDay() + response.getPressureNight()) / 2);
		TemperatureInfo temperature = new TemperatureInfo(averageTemperature, null, "celsius");
		WindInfo windInfo = new WindInfo((float) response.getWindSpeed(), null, null, "mps");
		PressureInfo pressureInfo = new PressureInfo(averagePressure, null, "mmHg");
		PrecipitationInfo precipitationInfo = new PrecipitationInfo(null, null, null, null,
				response.getPrecipitationProbability());
		HumidityInfo humidityInfo = null;
		CloudsInfo cloudsInfo = null;
		return Forecast.builder().date(LocalDate.parse(response.getDate()).atStartOfDay().toInstant(ZoneOffset.UTC))
				.weatherDescription(commentResolver.byId(response.getCommentId())).temperature(temperature)
				.wind(windInfo).pressure(pressureInfo).precipitation(precipitationInfo).humidity(humidityInfo)
				.clouds(cloudsInfo).build();
	}

	public List<Forecast> week(String response) {
		try {
			List<MeteoInfoResponse> raw = call(response);
			return raw.stream().map(this::convert).collect(Collectors.toList());
		} catch (RuntimeException ex) {
			throw new RuntimeException("Couldn't convert response from the provider", ex);
		}
	}
}
