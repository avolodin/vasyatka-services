package com.epam.vasyatka.weather.client.meteoinfo.connector.converter;

import com.epam.vasyatka.weather.integration.connector.ProviderLocationMapper;
import com.epam.vasyatka.weather.integration.model.exception.LocationNotSupportedException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class MeteoInfoLocationMapper implements ProviderLocationMapper {

	public static final String GET_SERVICE_ID_BY_NAME =
			"select serviceid from meteo_info_locations where lower(name) = ? limit 1";

	private final JdbcTemplate jdbc;

	public MeteoInfoLocationMapper(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	@Override
	public String id(String location) {
		try {
			Integer id = jdbc.queryForObject(GET_SERVICE_ID_BY_NAME,
					Integer.class,
					location.toLowerCase());
			return id.toString();
		} catch (DataAccessException ex) {
			throw new LocationNotSupportedException(location, ex);
		}
	}
}
