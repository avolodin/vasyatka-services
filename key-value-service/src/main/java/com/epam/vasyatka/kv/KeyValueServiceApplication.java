package com.epam.vasyatka.kv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.PATCH;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@SpringBootApplication
public class KeyValueServiceApplication {

	@Bean
	public RouterFunction<ServerResponse> router(KeyValueHandler handler) {
		return route(GET("/{userId}/{key}").and(accept(APPLICATION_JSON)), handler::get)
				.and(route(PATCH("/{userId}").and(accept(APPLICATION_JSON)), handler::patch));
	}

	public static void main(String[] args) {
		SpringApplication.run(KeyValueServiceApplication.class, args);
	}
}
