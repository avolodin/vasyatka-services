package com.epam.vasyatka.kv;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.Map;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.ServerResponse.accepted;
import static org.springframework.web.reactive.function.server.ServerResponse.notFound;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Service
public class KeyValueHandler {

	private final ReactiveHashOperations<String, String, String> ops;

	public KeyValueHandler(ReactiveHashOperations<String, String, String> ops) {
		this.ops = ops;
	}

	public Mono<ServerResponse> patch(ServerRequest request) {
		String userId = request.pathVariable("userId");
		return ops.putAll(userId, request.bodyToMono(new ParameterizedTypeReference<Map<String, String>>() {}).block())
				.then(accepted().build()); // FIXME: can it be done without blocking?
	}

	public Mono<ServerResponse> get(ServerRequest request) {
		String userId = request.pathVariable("userId");
		String key = request.pathVariable("key");
		return ops.get(userId, key)
				.flatMap(value -> ok().body(fromObject(Collections.singletonMap(key, value))))
				.switchIfEmpty(notFound().build());
	}
}
