#!/usr/bin/env bash

source vars.sh
ip=$(kubectl get ingress/gateway -o jsonpath='{..ip}')
echo 'Assigned IP of ingress/gateway is' ${ip}

if [ ${ip} ]
then
    gcloud compute addresses create nginx-ip \
        --addresses ${ip} \
        --region ${ip_zone}
else
    echo 'IP is not assigned, try a bit later'
fi