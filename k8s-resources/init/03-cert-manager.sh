#!/usr/bin/env bash

git clone https://github.com/jetstack/cert-manager
cd cert-manager
git checkout v0.2.3

helm install \
    --name cert-manager \
    --namespace kube-system \
    contrib/charts/cert-manager \
    --set rbac.create=false
