#!/usr/bin/env bash

kubectl apply -f ../tls/issuer-letsencrypt-prod.yml
kubectl apply -f ../tls/junky-solutions-certificate.yml
