#!/usr/bin/env bash

source vars.sh
gcloud compute disks create postgres-disk --type=pd-standard --size=10GB
gcloud compute disks create redis-disk --type=pd-standard --size=10GB
