#!/usr/bin/env bash

source vars.sh
gcloud config set project ${project_name}
gcloud config set compute/zone ${cluster_zone}
gcloud container clusters get-credentials ${cluster_name}
